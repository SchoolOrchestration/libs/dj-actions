# DJ Actions

[![PyPI version](https://badge.fury.io/py/dj-actions.svg)](https://badge.fury.io/py/dj-actions)

[[_TOC_]]

A pattern + tooling to enable a standardized way to interact with and mutate data on a Django model

## What's in the box

- [x] A standardized way to add custom actions to your models
- [x] Self documenting
- [x] Automated logging
- [x] Development API
- [x] Profit

## Getting started

### Installation

```
pip install dj-actions
```

Add `actions` to `INSTALLED_APPS`

### Prepare your app for action!

```bash
python manage.py start_actions
# answer the questions
```

### Add your first action

```bash
python manage.py create_action
# answer the questions
```

## Advanced setup:

```python
urlpatterns = [
    ..
    # expose actions on the actions.models.ActionEvent
    url(r'^actions/', include(action_router.urls)),
    # interactive UI / docs
    path('admin/actions/ui/', include('actions.urls')),
    ..
]
```

### Configuration

* `DJACTION_TRACK_ACTIONS` if `True` will store action info in the db (default: `True`)
* `DJACTION_TTL_MINUTES` actions older than this will be archived (default 1 day (60*24))

## UI

> `dj-actions` includes a UI playground for interacting with your actions

![UI Screenshot](https://dropbox-appointmentguru.s3.eu-central-1.amazonaws.com/dj-actions.gif)

To include the UI, to your urls.py add:

```python
urlpatterns = [
    ...
    path('admin/actions/ui/', include('actions.urls')),
    ...
]
```

Then you need to tell the UI about your resources. Add something like this to `settings.py`:

```
DJACTION_RESOURCES = [
    {
        "name": "Todo",
        "path": "/todos/"
    },
    {
        "name": "Action Events",
        "path": "/actions/actionevents/"
    }
]
```



## Features

* Reliable, simple pattern to execute complex logic
* Self documenting

## Tooling

This library provides you with some tools and tasks which might be useful:

### Stream,io (WIP)

> Push action information into stream.io





## Concepts

### Task Registry

The Task Registry provides a means of defining tasks in a self documenting way.
Based on the meta-data provided in the task registry, tasks will be exposed in various different ways


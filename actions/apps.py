from django.apps import AppConfig


class ActionsAppConfig(AppConfig):
    name = 'actions'

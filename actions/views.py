from django.shortcuts import render
from django.conf import settings


def index(request):
    context = {
        "resources": getattr(settings, 'DJACTION_RESOURCES', [])
    }
    return render(request, "djactions/app.html", context=context)

from django.contrib import admin
from .models import ActionEvent


class ActionEventAdmin(admin.ModelAdmin):
    list_display = (
        'actor_id',
        'action_id',
        'status_code',
        'complete',
        'duration',
    )
    list_filter =(
        'status_code',
        'action_id',
    )


admin.site.register(ActionEvent, ActionEventAdmin)

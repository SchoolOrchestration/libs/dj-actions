from django.db import models


TODO_STATUSES = [
    ('N', 'Todo'),
    ('P', 'In Progress'),
    ('D', 'Done'),
]


class Todo(models.Model):

    def __str__(self):
        return self.title

    owner = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    status = models.CharField(max_length=30, choices=TODO_STATUSES, default='N') # noqa 501
    size = models.PositiveIntegerField(default=1)
    due = models.DateField(auto_now=True)
    due_time = models.DateTimeField(auto_now=True)

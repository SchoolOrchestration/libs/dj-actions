from django import forms
from django.conf import settings

# resource actionform provides deault functionality for this resource:
# from example_app.actions.mixins import TodoActionFormMixin
from actions.forms.mixins import ActionFormMixin
from actions.fields import CommaSeparatedCharField

class BulkMarkDoneForm(ActionFormMixin, forms.Form):
    """
    action_id: bulk_mark_done
    """
    action_id = 'todo:bulk_mark_done'
    # fields go here:
    ids = CommaSeparatedCharField(required=True)

    def get_stream_message(self, instance):
        return 'performed bulk_mark_done on {}'.format(
            instance.pk
        )

    def save(self, request, pk=None):
        ids = self.cleaned_data.get('ids')
        todos = Todo.objects.get(pk__in=ids)

        # Implement your action here
        todos.update(
            status="D"
        )

        meta = {}
        return (meta, todos)

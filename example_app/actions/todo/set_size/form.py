from django import forms
from django.conf import settings

# resource actionform provides deault functionality for this resource:
# from example_app.actions.mixins import TodoActionFormMixin
from actions.forms.mixins import ActionFormMixin
from example_app.models import Todo


class SetSizeForm(ActionFormMixin, forms.Form):
    """
    action_id: set_size
    """
    action_id = 'todo:set_size'
    size = forms.IntegerField(required=True, min_value=1, max_value=3)

    def get_object(self, pk):
        return Todo.objects.get(id=pk)

    def get_stream_message(self, instance):
        return 'performed set_size on {}'.format(
            instance.pk
        )

    def save(self, request, pk=None):

        todo = self.get_object(pk)
        todo.size = self.cleaned_data.get('size')
        todo.save()
        meta = {}
        return (meta, todo)

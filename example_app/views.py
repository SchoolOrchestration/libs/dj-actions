from .models import Todo
from rest_framework import (
    routers,
    viewsets,
    serializers
)

from actions.mixins import ActionMixin
from .actions.todo.action_registry import ACTIONS


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = '__all__'


class TodoViewSet(ActionMixin, viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    action_registry = ACTIONS


router = routers.DefaultRouter()
router.register(r'todos', TodoViewSet)

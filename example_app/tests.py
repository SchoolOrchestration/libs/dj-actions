from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from .models import Todo
import json

class ActionMixinTestCase(TestCase):

    def setUp(self):
        user = get_user_model().objects.create(username='joe', password='testtest')
        Todo.objects.create(owner=user, title='foo')
        Todo.objects.create(owner=user, title='bar')
        Todo.objects.create(owner=user, title='baz')

    # def test_perform_action(self):
    #     url = reverse('todo-dispatch')
    #     data = {
    #         "foo": "bar"
    #     }
    #     res = self.client.post(
    #         url + "?id=complete",
    #         data=json.dumps(data),
    #         content_type = "application/json"
    #     )
    #     self.assertEqual(res.status_code, 201)

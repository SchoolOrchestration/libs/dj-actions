#!/usr/bin/env bash
echo "==================================================================="
echo "STARTING TESTS"
echo "==================================================================="
coverage run manage.py test
echo "Generate reports"
coverage report

"""
python manage.py test {{base_directory| replace("/",".")}}.tests.actionforms.test_{{action_id}}_form
"""

from django.test import TestCase
from {{base_directory| replace("/",".")}}.actions.{{action_id}}.form import {{camel_action_id}}Form
from {{base_directory| replace("/",".")}}.actions.action_registry import ACTIONS
from actions.util import get_action_form
from testutils import (
    create_fake_appointment,
    create_fake_practitioner
)

from django.test.client import RequestFactory
import mock

# BulkAddCodesForm
# use this constant rather than hard importing the form so
# that this code can operate based solely on the registry
ACTION_FORM = get_action_form(ACTIONS, '{{action_id|lower}}')

class Test{{camel_action_id}}FormTestCase(TestCase):

    def submit_form(self, user, payload, pk=None, url='/', expect_errors=False):
        rf = RequestFactory()
        req = rf.post(url, payload)
        req.user = user
        form = ACTION_FORM(payload)
        if form.is_valid():
            return form.save(req, pk)
        else:
            if not expect_errors:
                assert False, 'Did not expect to get form errors. Got: {}'.format(form.errors)

    def setUp(self):
        self.p1 = create_fake_practitioner()
        self.p2 = create_fake_practitioner()
        self.appt = create_fake_appointment(practitioner=self.p1, **{"create_process": False})
        self.appt1 = create_fake_appointment(practitioner=self.p1, **{"create_process": False})

    def test_successful_form_submission(self):
{% if action_type == 'bulk' %}
        payload = { "{{resource|lower}}s": "{},{}".format(self.appt.id, self.appt1.id)}
        res = self.submit_form(self.p1, payload)
{% endif %}
{% if action_type == 'instance' %}
        payload = {}
        res = self.submit_form(self.p1, payload, pk=self.appt.pk)
{% endif %}

        import ipdb; ipdb.set_trace()
        # assert ...

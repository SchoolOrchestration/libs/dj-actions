# {{action_id}}

* **Location:** {{action_directory}}
* **Action registry endpoint:** [context]{{resource|lower}}/actions/list/
* **Action documentation endpoint:** [context]{{resource|lower}}/actions/{{action_id|lower}}/

## Run tests

```bash
python manage.py test {{base_directory| replace("/",".")}}.actions.{{action_id}}
```

## Getting started

* Implement you action in the `save` method of the form at `{{base_directory}}form.py`
* Test your action from `{{base_directory}}/tests/test_form.py`

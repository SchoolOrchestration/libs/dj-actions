from django.test import TestCase
from actions.tests.testutils import (
    get_instance_action_url,
    get_bulk_action_url
)
from testutils import (
    create_fake_appointment,
    create_fake_practitioner,
    get_proxy_headers
)

class Test{{camel_action_id}}ApiTestCase(TestCase):

    def perform_action(self, action_id, payload, performed_by):
{% if action_type == 'bulk' %}
        url = get_bulk_action_url(
            self.resource,
            action_id,
            context = self.context
        )
        return self.client.post(
            url,
            data = payload,
            context_type='application/json',
            **get_proxy_headers(performed_by.id)
        )
{% endif %}
{% if action_type == 'instance' %}
        url = get_instance_action_url(
            self.resource,
            self.appt.id,
            action_id,
            context = self.context
        )
        return self.client.post(
            url,
            data = payload,
            context_type='application/json',
            **get_proxy_headers(performed_by.id)
        )
{% endif %}

    def setUp(self):
        self.resource = '{{resource|lower}}s'
        self.context = '{{default_context}}'

        self.p1 = create_fake_practitioner()
        self.p2 = create_fake_practitioner()
        self.appt = create_fake_appointment(
            practitioner=self.p1,
            **{"create_process": False}
        )
        self.appt2 = create_fake_appointment(
            practitioner=self.p1,
            **{"create_process": False}
        )

    def test_basic_send(self):
{% if action_type == 'bulk' %}
        payload = {
            "appointments": "{},{}".format(self.appt.id, self.appt2.id)
        }
{% endif %}
{% if action_type == 'instance' %}
        payload = {}
{% endif %}
        res = self.perform_action('{{action_id}}', payload, performed_by=self.p1)
        import ipdb; ipdb.set_trace()
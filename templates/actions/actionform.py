from django import forms
from django.conf import settings

# resource actionform provides deault functionality for this resource:
from {{base_directory| replace("/",".")}}.actions.mixins import {{resource}}ActionFormMixin
from actions.forms.mixins import ActionFormMixin
from fields import CommaSeparatedCharField


class {{camel_action_id}}Form({{resource}}ActionFormMixin, ActionFormMixin, forms.Form):
    """
    action_id: {{action_id}}
    """
    action_id = '{{resource|lower}}:{{action_id}}'
    {% if action_type == 'bulk' %}{{resource|lower}}s = CommaSeparatedCharField(required=True){% endif %}
    # fields go here:
    # ...

    def get_stream_message(self, {{resource|lower}}):
        return '{{action_id}} {{resource|lower}} {}'.format(
            {{resource|lower}}.title
        )

    def save(self, request, pk=None):

{% if action_type == 'bulk' %}
        {{resource|lower}}s = self.get_{{resource|lower}}s()
        self.verify_has_permissions(request.user, {{resource|lower}}s)

        ## Implement your action here
        ## ..

        for {{resource|lower}} in {{resource|lower}}s:
            self.perform_post_process(request, {{resource|lower}})
{% endif %}
{% if action_type == 'instance' %}
        {{resource|lower}} = self.get_object(pk)
        self.verify_has_permissions(request.user, [{{resource|lower}}])

        ## Implement your action here
        ## ..

        self.perform_post_process(request, {{resource|lower}})
{% endif %}
        # extra info to send back to the client, e.g.: count, batch_id etc
        meta = {}
        return (meta, {{resource|lower}}{% if action_type == 'bulk' %}s{% endif %})